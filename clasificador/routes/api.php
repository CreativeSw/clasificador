<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
Use App\Http\Controllers\ApiController;
Use App\Http\Controllers\ReglasController;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('',function(){
  return array('success'=>true);
});



Route::get('allsource',[ApiController::class,'origendatos']);
Route::get('onesource/{origen}',[ApiController::class,'campos']);
Route::get('onesource/Ivr/{nombre}',[ApiController::class,'preguntasencuenta']);

Route::post('prerule',[ApiController::class,'showdataregla']);

Route::get('allrules',[ReglasController::class,'index']);
Route::post('createroule',[ApiController::class,'createrule']);
Route::get('onerule/{id}',[ReglasController::class,'show']);
Route::get('deleterule/{id}',[ReglasController::class,'destroy']);

Route::get('editrule/{id}',[ApiController::class,'editrule']);
Route::post('saverule/{id}',[ApiController::class,'saverule']);

Route::get('getdatos',[ApiController::class,'getdatos']);
