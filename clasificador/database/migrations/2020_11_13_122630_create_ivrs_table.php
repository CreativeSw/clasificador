<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIvrsTable extends Migration
{

    public function up()
    {
        Schema::create('ivrs', function (Blueprint $table) {
            $table->id();
            $table->string('campana');
            $table->string('status');
            $table->string('pregunta');
            $table->string('respuesta');
            $table->string('telefono');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('ivrs');
    }
}
