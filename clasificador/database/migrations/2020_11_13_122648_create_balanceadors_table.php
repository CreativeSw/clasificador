<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBalanceadorsTable extends Migration
{

    public function up()
    {
        Schema::create('balanceadors', function (Blueprint $table) {
            $table->id();
            $table->string('url');
            $table->date('datetime');
            $table->integer('cant_mg');
            $table->integer('cant_comentarios');
            $table->integer('cant_compartidos');
            $table->string('descripcion_post');
            $table->string('pag_noticia');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('balanceadors');
    }
}
