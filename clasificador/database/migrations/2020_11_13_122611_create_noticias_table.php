<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticiasTable extends Migration
{

    public function up()
    {
        Schema::create('noticias', function (Blueprint $table) {
            $table->id();
            $table->string('titulo');
            $table->string('link');
            $table->string('noticiascol');
            $table->string('texto',2000);
            $table->string('medio');
            $table->date('fecha');
            $table->date('fecha2');
            $table->string('location3');
            $table->string('location100');
            $table->string('location200');
            $table->string('sentimiento');
            $table->integer('salud');
            $table->integer('trabajo');
            $table->integer('educacion');
            $table->integer('sentiminto');
            $table->string('location300');
            $table->integer('imagen');
            $table->string('grupo');
            /*
            location3   | varchar(45)  | YES  |     | NULL    |                |
| location100 | varchar(45)  | YES  |     | NULL    |                |
| location200 | varchar(45)  | YES  |     | NULL    |                |
| sentimiento | varchar(45)  | YES  |     | NULL    |                |
| salud       | int(11)      | YES  |     | NULL    |                |
| trabajo     | int(11)      | YES  |     | NULL    |                |
| educacion   | int(11)      | YES  |     | NULL    |                |
| sentiminto  | int(11)      | YES  |     | NULL    |                |
| location300 | varchar(45)  | YES  |     | NULL    |                |
| imagen      | int(5)       | YES  |     | NULL    |
            */
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('noticias');
    }
}
