<?php

namespace Database\Factories;

use App\Models\Ivr;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Provider\en_US\PhoneNumber;
class IvrFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ivr::class;


    public function definition()
    {
      $campana = ['camp1','camp2','otra'];
      $status = ['finalizada','en proceso'];
      $pregunta = ['toma','bebe','salud'];
      $respuesta = ['si','no','nose'];

        return [
          'campana' => $campana[array_rand($campana)],
          'status'=>$status[array_rand($status)],
          'pregunta'=>$pregunta[array_rand($pregunta)],
          'respuesta'=>$respuesta[array_rand($respuesta)],
          'telefono'=>rand(200,300)
        ];
    }
}
