<?php

namespace Database\Factories;

use App\Models\Balanceador;
use Illuminate\Database\Eloquent\Factories\Factory;

class BalanceadorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Balanceador::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'url'=>$this->faker->url,
            'datetime'=> date("Y-m-d H:i:s"),
            'cant_mg'=> rand(0,4567),
            'cant_comentarios'=> rand(0,4567),
            'cant_compartidos'=> rand(0,4567),
            'descripcion_post'=> $this->faker->text,
            'pag_noticia'=> $this->faker->url
        ];
    }
}
