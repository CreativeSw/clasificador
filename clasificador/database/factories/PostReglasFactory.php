<?php

namespace Database\Factories;

use App\Models\Reglas;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReglasFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Reglas::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'nombre'=>$this->faker->name,
            'noticiasquery'=>'queryfaker',
            'ivrquery'=>'queryfaker',
            'balanceadorquery'=>'queryfaker',
            'data'=>'queryfaker'

        ];
    }
}
