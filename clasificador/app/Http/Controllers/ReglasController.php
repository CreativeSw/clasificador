<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reglas;
use App\Models\Balanceador;
use App\Models\Noticias;
use App\Models\Ivr;
class ReglasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $regalas = Reglas::all();
      return array('success'=>true,
                  'data'=>$regalas
                );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
      $rule = Reglas::find($id);
      if($rule == null){
        return array('success'=>false,'data'=>'No existe la regla');
      }else{
        if($rule->ivrquery != ""){
          $datos['data']['Ivr'] = json_decode($rule->ivrquery,true);
          foreach ($datos['data']['Ivr'] as $key => $v) {
            if($v == null)
              unset($datos['data']['Ivr'][$key]);
          }
          $ivrquery = json_encode($datos['data']['Ivr']);
          $cquery = Ivr::query();
          foreach($datos['data']['Ivr'] as $key => $value){
            $cquery = $cquery->where($key,$value);
          }
          $ivr = $cquery->paginate(3000);
        }
        if($rule->balanceadorquery != ""){
          $datos['data']['Balanceador'] = json_decode($rule->balanceadorquery,true);
          $bquery = Balanceador::query();
          foreach($datos['data']['Balanceador'] as $key => $value){
            $bquery = $bquery->where($key,$value);
          }
          $balanceador = $bquery->paginate(3000);
        }
        if($rule->noticiasquery != ""){
          //dd('noticia');
          $datos['data']['Noticias'] = json_decode($rule->noticiasquery,true);
          foreach ($datos['data']['Noticias'] as $key => $v) {
            if($v == null)
              unset($datos['data']['Noticias'][$key]);
          }
          //$noticiasquery = json_encode($datos['data']['Noticias']);
          $nquery = Noticias::query();
          $medio = [];
          $sentimiento = [];
          if(isset($datos['data']['Noticias']['medio'])){
            foreach ($datos['data']['Noticias']['medio'] as $key =>$v) {
                $medio[] = $v;
            }
            if(count($datos['data']['Noticias']['medio'])>0){
              unset($datos['data']['Noticias']['medio']);
            }
          }
          if(isset($datos['data']['Noticias']['sentimiento'])){
            foreach ($datos['data']['Noticias']['sentimiento'] as $key=> $v) {
                $sentimiento[] = $v;
            }
            if(count($datos['data']['Noticias']['sentimiento'])>0){
              unset($datos['data']['Noticias']['sentimiento']);
            }
          }
          if(isset($datos['data']['Noticias']['texto'])){
            $nquery = $nquery->where('texto', 'like', '%' . $datos['data']['Noticias']['texto'] . '%');
            unset($datos['data']['Noticias']['texto']);
          }
          foreach ($datos['data']['Noticias'] as $key => $value) {
            $nquery = $nquery->where($key,$value);
          }
          if(count($medio)> 0){
            $nquery = $nquery->whereIn('medio',$medio);
          }
          if(count($sentimiento)>0){
            $nquery = $nquery->whereIn('sentimiento',$sentimiento);
          }
          $noticias = $nquery->paginate(3000);
        }
        $respuesta = array('success'=>true,
          'data'=>[]
        );
        if(isset($datos['data']['Noticias'])){
          $label = [
            'titulo',
            'texto',
            'medio',
            'fecha',
            'fecha2',
            'sentimiento',
            'salud',
            'trabajo',
            'educacion',
            'sentiminto',
          ];
          $respuesta['data']['Noticias']= array(
            'tablehead'=>$label,
            'tablebody'=>$noticias
          );
        }

        if(isset($datos['data']['Balanceador'])){
          $label = [
            'url',
            'datetime',
            'cant_mg',
            'cant_comentarios',
            'cant_compartidos',
            'descripcion_post',
            'pag_noticia'
          ];
          $respuesta['data']['Balanceador'] = array(
            'tablehead'=>$label,
            'tablebody'=>$balanceador
          );
        }


        if(isset($data['data']['Ivr'])){
          $label = [
            'campaña',
            'status',
            'pregunta',
            'respuesta',
            'telefono'
          ];
          $respuesta['data']['Ivr'] = array('tablehead' => $label,
          'tablebody'=>$ivr );
        }
        return $respuesta;
        dd($rule);
      }
    }

    /*
    public function viewrule($id){

    }

    *
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $rule = Reglas::find($id);
      if($rule == null){
        return array('success'=>false,'data'=>'No existe la regla');
      }else{
        $rule->delete();
        return array('success'=>true,'data'=>'La regla fue borrada con exito');
      }
    }



}
