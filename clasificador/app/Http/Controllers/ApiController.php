<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Noticias;
use App\Models\Balanceador;
use App\Models\Ivr;
use App\Models\Reglas;
use DB;
class ApiController extends Controller
{
  public function origendatos(){
      return array(
          'success'=>true,
          'data'=>array('Noticias','Ivr','Balanceador')
      );
    }

    public function campos($origen){
      if($origen != 'Noticias' && $origen != 'Ivr' && $origen != 'Balanceador'){
        return array(
            'success'=>false,
            'data'=>'Origen de Datos Inexistente'
          );
      }else{
        $respuesta = ['success'=>true,'data'=>[]];
        switch ($origen) {
          case 'Noticias':
            $data = $this->noticiascampos();
            $medio = Noticias::All()->groupby('medio');
            foreach ($medio as $key => $m) {
              $data['medio']['valor'][] = $key;
            }
            $sentimiento = Noticias::All()->groupby('sentimiento');
            foreach ($sentimiento as $key => $m) {
              $data['sentimiento']['valor'][] = $key;
            }
            break;
          case 'Ivr':
            $data = $this->ivrcampos();
            $campana = Ivr::all()->groupby('campana');
            foreach ($campana as $key => $m) {

              $data['campana']['valor'][] = $key;
            }
            $status = Ivr::all()->groupby('status');
            foreach ($status as $key => $value) {
              $data['status']['valor'][] = $key;
            }
            break;
          case 'Balanceador':
            $data = $this->balanceadorcampos();
            break;
          default:
            break;
        }
        $respuesta['data'] = $data;
        return $respuesta;
      }
    }

    public function preguntasencuenta($encuesta){
      $encuesta = Ivr::where('campana',$encuesta)->get();
      $duplicado = [];
      $respuesta = [];
      foreach ($encuesta as $e) {
        $duplicado[] = $e->pregunta;
        $respuesta[] = $e->respuesta;
      }
      $lisrsp = [] ;
      foreach (array_unique($respuesta) as $p) {
          $lisrsp[] = $p;
      }

      $listdup = [];
      foreach (array_unique($duplicado) as $v) {
        $listdup[] = $v;
        // code...
      }


      return array(
          'success'=>true,
          'data'=>array(
            'pregunta'=>array(
              'tipo'=>'select',
              'valor'=>$listdup
            ),
            'respuesta'=>array(
              'tipo'=>'select',
              'valor'=>$lisrsp
            ),
          )
      );
    }
    public function getdatos(){
      $idnoticia = Noticias::orderBy('id', 'desc')->first();
      if($idnoticia == null){
        $idnoticia = 0;
      }else{
        $idnoticia = $idnoticia->id;
      }
      $idbalanceador = Balanceador::orderBy('id', 'desc')->first();
      if($idbalanceador == null){
        $idbalanceador = 0;
      }else{
        $idbalanceador = $idbalanceador->id;
      }

      $idivr = Ivr::orderBy('id', 'desc')->first();
      if($idivr == null){
        $idivr = 0;
      }else{
        $idivr = $idivr->id;
      }
      $balanceador = mysqli_connect("167.86.120.98", "Daniel", '!_32$$aG', "data_noticias",3307);
      $query = "select * from noticia";
      $resultado = $balanceador->query($query);
      while($row = $resultado->fetch_array(MYSQLI_ASSOC)){
        $rows[] = $row;
      }
      foreach($rows as $row){
        $existe =Balanceador::find($row['id_noticia']);
        if($existe== null){
         Balanceador::create(array(
            'url'=>$row['url'],
            'datetime'=>$row['datetime'],
            'cant_mg'=>$row['cant_mg'],
            'cant_comentarios'=>$row['cant_comentarios'],
            'cant_compartidos'=>($row['cant_compartidos']==null)?0:$row['cant_compartidos'],
            'descripcion_post'=>($row['descripcion_post']==null)?'':$row['descripcion_post'],
            'pag_noticia'=>$row['pag_noticia']
          ));
        }
      }
      mysqli_close($balanceador);

      $noticias = mysqli_connect("167.86.120.98", "Daniel", '!_32$$aG', "noticias_sergio",3307);
      $query = "select * from noticias";
      $re = $noticias->query($query);
      $rows = [];
      while($row = $re->fetch_array(MYSQLI_ASSOC)){
        $rows[] = $row;
      }
      foreach($rows as $row){
        $existe = Noticias::find($row['id']);
        if($existe == null){
          Noticias::create(array(
            'titulo'=>($row['titulo']==null)?'':$row['titulo'],
            'link'=>($row['link']==null)?'':$row['link'],
            'noticiascol'=>($row['noticiascol']==null)?'':$row['noticiascol'],
            'texto'=>($row['texto']==null)?'':$row['texto'],
            'medio'=>($row['medio']==null)?'':$row['medio'],
            'fecha'=>($row['fecha']==null)?'':$row['fecha'],
            'fecha2'=>($row['fecha2']==null)?'':$row['fecha2'],
            'location3'=>($row['location3']==null)?'':$row['location3'],
            'location100'=>($row['location100']==null)?'':$row['location100'],
            'location200'=>($row['location200']==null)?'':$row['location200'],
            'sentimiento'=>($row['sentimiento']==null)?'':$row['sentiminto'],
            'salud'=>($row['salud']==null)?'':$row['salud'],
            'trabajo'=>($row['trabajo']==null)?'':$row['trabajo'],
            'educacion'=>($row['educacion']==null)?'':$row['educacion'],
            'sentiminto'=>($row['sentiminto']==null)?'':$row['sentiminto'],
            'location300'=>($row['location300']==null)?'':$row['location300'],
            'imagen'=>($row['imagen']==null)?'':$row['imagen'],
            'grupo'=>($row['grupo']==null)?'':$row['grupo'],
          ));
        }
      }

      mysqli_close($noticias);
      /*
      $balanceador = DB::connection('balanceador')->select('select * from noticas');
      $noticias = DB::connection('noticias')->table('noticias')->where('id','>',$idnoticia)->get();
      dd($balanceador);*/
      //$ivrs = DB::connection('ivr')->table('ivr')->where('id','>',$idivr)->get();


      //dd($b->get());
      //DB::connection('mysql2')
    }
    public function showdataregla(Request $request){
      $validar = $request->validate([
        'data'=>'required'
      ]);
      $datos = request()->all();

      $datosold = $datos;
      $balanceadorquery = '';
      $noticiasquery = '';
      $ivr = '';
      $ivrquery = '';
      if(isset($datos['data']['Ivr'])){
        foreach ($datos['data']['Ivr'] as $key => $v) {
          if($v == null)
            unset($datos['data']['Ivr'][$key]);
        }
        $ivrquery = json_encode($datos['data']['Ivr']);
        $cquery = Ivr::query();
        foreach($datos['data']['Ivr'] as $key => $value){
          $cquery = $cquery->where($key,$value);
        }
        $ivr = $cquery->paginate(3000);
      }

      if(isset($datos['data']['Balanceador'])){
        foreach ($datos['data']['Balanceador'] as $key => $v) {
          if($v == null)
            unset($datos['data']['Balanceador'][$key]);
        }
        $balanceadorquery = json_encode($datos['data']['Balanceador']);
        $bquery = Balanceador::query();
        foreach($datos['data']['Balanceador'] as $key => $value){
          $bquery = $bquery->where($key,$value);
        }
        $balanceador = $bquery->paginate(3000);
      }

      if(isset($datos['data']['Noticias'])){
        foreach ($datos['data']['Noticias'] as $key => $v) {
          if($v == null)
            unset($datos['data']['Noticias'][$key]);
        }
        $noticiasquery = json_encode($datos['data']['Noticias']);
        $nquery = Noticias::query();
        $medio = [];
        $sentimiento = [];
        if(isset($datos['data']['Noticias']['medio'])){
          foreach ($datos['data']['Noticias']['medio'] as $key =>$v) {
              $medio[] = $v;
          }
          if(count($datos['data']['Noticias']['medio'])>0){
            unset($datos['data']['Noticias']['medio']);
          }
        }
        if(isset($datos['data']['Noticias']['sentimiento'])){
          foreach ($datos['data']['Noticias']['sentimiento'] as $key=> $v) {
              $sentimiento[] = $v;
          }
          if(count($datos['data']['Noticias']['sentimiento'])>0){
            unset($datos['data']['Noticias']['sentimiento']);
          }
        }
        if(isset($datos['data']['Noticias']['texto'])){
          $nquery = $nquery->where('texto', 'like', '%' . $datos['data']['Noticias']['texto'] . '%');
          unset($datos['data']['Noticias']['texto']);
        }
        foreach ($datos['data']['Noticias'] as $key => $value) {
          $nquery = $nquery->where($key,$value);
        }
        if(count($medio)> 0){
          $nquery = $nquery->whereIn('medio',$medio);
        }
        if(count($sentimiento)>0){
          $nquery = $nquery->whereIn('sentimiento',$sentimiento);
        }
        $noticias = $nquery->paginate(3000);
      }

      $respuesta = array('success'=>true,
        'data'=>[],
        'olddata'=>$datosold,
        'noticiasquery'=>$noticiasquery,
        'balanceadorquery'=>$balanceadorquery,
        'ivrquery'=>$ivrquery
      );
      if(isset($datos['data']['Noticias'])){
        $label = [
          'titulo',
          'texto',
          'medio',
          'fecha',
          'fecha2',
          'sentimiento',
          'salud',
          'trabajo',
          'educacion',
          'sentiminto',
        ];
        $respuesta['data']['Noticias']= array(
          'tablehead'=>$label,
          'tablebody'=>$noticias
        );
      }

      if(isset($datos['data']['Balanceador'])){
        $label = [
          'url',
          'datetime',
          'cant_mg',
          'cant_comentarios',
          'cant_compartidos',
          'descripcion_post',
          'pag_noticia'
        ];
        $respuesta['data']['Balanceador'] = array(
          'tablehead'=>$label,
          'tablebody'=>$balanceador
        );
      }


      if(isset($data['data']['Ivr'])){
        $label = [
          'campaña',
          'status',
          'pregunta',
          'respuesta',
          'telefono'
        ];
        $respuesta['data']['Ivr'] = array('tablehead' => $label,
        'tablebody'=>$ivr );
      }
      return $respuesta;
    }

    public function createrule(){
      $datos = request()->all();
      $data = json_encode($datos['data']);

      if($datos['balanceadorquery'] == null)
        $datos['balanceadorquery']='';
      if($datos['noticiasquery'] == null)
        $datos['noticiasquery']='';
      if($datos['ivrquery'] == null)
        $datos['ivrquery']='';

      Reglas::create(array(
          'nombre'=>$datos['nombre'],
          'noticiasquery'=>$datos['noticiasquery'],
          'ivrquery' => $datos['ivrquery'],
          'balanceadorquery'=>$datos['balanceadorquery'],
          'data'=>$data
      ));
      return array('success'=>true,
                    'mensaje'=>'Fue creado con exito');
    }


    public function editrule($id){
      $rule = Reglas::find($id);
      if($rule == null){
        return array('success'=>false,'data'=>'No existe la regla');
      }else{
        return array('success'=>true,'data'=>$rule);
      }
    }

    public function saverule($id){
      if($rule == null){
        return array('success'=>false,'data'=>'No existe la regla');
      }else{
        dd(request()->all());
      }
    }




    private function noticiascampos(){
      return array(
        'texto'=>array(
          'tipo'=>'text',
          'valor'=>''
        ),
        'medio'=>array(
          'tipo'=>'select',
          'valor'=>array()
        ),
        'fecha'=>array(
          'tipo'=>'date',
          'valor'=>''
        ),
        'fecha2'=>array(
          'tipo'=>'date',
          'valor'=>''
        ),
        'sentimiento'=>array(
          'tipo'=>'select',
          'valor'=>array()
        ),
        'salud'=>array(
          'tipo'=>'number',
          'valor'=>array(
            'min'=>1,
            'max'=>10
          )
        ),
        'trabajo'=>array(
          'tipo'=>'number',
          'valor'=>array(
            'min'=>1,
            'max'=>10
          )
        ),
        'educacion'=>array(
          'tipo'=>'number',
          'valor'=>array(
            'min'=>1,
            'max'=>10
          )
        ),
        'sentiminto'=>array(
          'tipo'=>'number',
          'valor'=>array(
            'min'=>1,
            'max'=>10
          )
        ),
      );
    }

    private function balanceadorcampos(){
      return array(
        'url'=>array(
          'tipo'=>'text',
          'valor'=>''
        ),
        'datetime'=>array(
          'tipo'=>'date',
          'valor'=>''
        ),
        'cant_mg'=>array(
          'tipo'=>'number',
          'valor'=>array(
            'min'=>1,
            'max'=>-1
          )
        ),
        'cant_comentarios'=>array(
          'tipo'=>'number',
          'valor'=>array(
            'min'=>1,
            'max'=>-1
          )
        ),
        'descripcion_post'=>array(
          'tipo'=>'text',
          'valor'=>''
        ),
        'pag_noticia'=>array(
          'tipo'=>'text',
          'valor'=>''
        ),
      );
    }

    private function ivrcampos(){
      return [
        'campana'=>array(
          'tipo'=>'select',
          'valor'=>array()
        ),
        'status'=>array(
          'tipo'=>'select',
          'valor'=>array()
        ),
        'pregunta'=>array(
          'tipo'=>'select',
          'valor'=>array()
        ),
        'respuesta'=>array(
          'tipo'=>'select',
          'valor'=>array()
        ),
        'telefono'=>array(
          'tipo'=>'text',
          'valor'=>''
        ),
      ];
    }
}
