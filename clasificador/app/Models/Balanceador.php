<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Balanceador extends Model
{
    use HasFactory;

    protected $fillable = [
      'url',
      'datetime',
      'cant_mg',
      'cant_comentarios',
      'cant_compartidos',
      'descripcion_post',
      'pag_noticia'
    ];
}
