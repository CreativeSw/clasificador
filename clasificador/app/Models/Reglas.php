<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reglas extends Model
{
    use HasFactory;
    protected $fillable = [
      'nombre',
      'noticiasquery',
      'ivrquery',
      'balanceadorquery',
      'data'
    ];
}
