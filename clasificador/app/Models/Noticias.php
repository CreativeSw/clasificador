<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Noticias extends Model
{
    use HasFactory;
    protected $fillable = [
      'titulo',
      'link',
      'noticiascol',
      'texto',
      'medio',
      'fecha',
      'fecha2',
      'location3',
      'location100',
      'location200',
      'sentimiento',
      'salud',
      'trabajo',
      'educacion',
      'sentiminto',
      'location300',
      'imagen',
      'grupo',
    ];

}
